#!/bin/bash

# Versión 1.0
# Este script se encarga de inicializar el bucket que va a almacenar el estado remoto de Terraform de un elemento en particular.

# Comprobamos una serie de prerrequisitos: awk, sed y terraform
if ! [[ -x $(command -v awk) ]] || ! [[ -x $(command -v sed) ]] || ! [[ -x $(command -v terraform) ]]
then
	echo "Faltan requisitos por cumplir"
	exit 1
fi

# Comprobamos si el número de argumentos es el correcto o si el directorio a crear ya existe
if [[ "$#" -ne 1 ]]
then
	echo "Número ilegal de argumentos. Solo 1"
	exit 1
fi

# Variables a utilizar.
# El script se encarga de leer el nombre del proyecto y generar de forma dinámica un bucket utilizando el nombre del proyecto como base
GCP_PROJECT=$(awk '/project = "/,/"/' provider.tf | awk -F \" '{ print $2 }')
GCP_BUCKET="tf-state-${GCP_PROJECT}"

if [[ ${GCP_PROJECT} == "" ]]
then
	echo "El proyecto está vacío. Rellene el fichero provider.tf con el nombre del proyecto antes de continuar"
	exit 1
else
	# Comprobamos si existe o no el directorio a crear y generamos la estructura de ficheros necesaria. Se comprueba si ya existe por si se están utilizando los ejemplos
	if ! [[ -d "$1" ]]
	then
		mkdir $1
		echo "# Recursos relacionados con $1" > $1/main.tf
		echo "# Variables relacionadas con $1" > $1/vars.tf
	fi

	if [[ -d "$1" ]]
	then
		cp provider.tf $1/provider.tf
		cp remote_state.tf $1/remote_state.tf
	fi

	# Configuramos un nuevo fichero remoto para los nuevos elementos y generar evitar el mayor número de interdependencias posible
	cd $1
	sed -i -e 's/bucket = ""/bucket = \"'"$GCP_BUCKET"'\"/g' remote_state.tf
	sed -i -e 's/prefix = "global\/backend.tfstate"/prefix = \"'"$1"'\/terraform.tfstate\"/g' remote_state.tf
	
	# Inicializamos Terraform en un fichero de estado diferente
	terraform init
	
	echo ""
	echo "$1 ha sido creado y configurado"
fi
