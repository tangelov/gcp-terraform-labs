# Recursos relacionados con gke
data "google_project" "project" {}

data "google_compute_network" "gcp_network" {
  name = "${var.gcp_network}"
}

# Recursos importados de tfstates remotos -- DESCOMENTAR Y RELLENAR CORRECTAMENTE
# data "terraform_remote_state" "kms" {
#  backend = "gcs"

#  config {
#    bucket = ""
#    prefix = "kms/terraform.tfstate"
#  }
#}

data "google_kms_secret" "k8s_password" {
  crypto_key = "${data.terraform_remote_state.kms.gcp-test-lab-key}"
  ciphertext = "CiQAHejTO6KDAzrlXPqWFlWgS2AHDmVhYr8+gpGyubHpSk167NwSRQAvb0oe/lvFmIiF9yZNW3j9IeYZ9+pZkVaX/zP8Q4ilxZ4HkzJZadaN/nlD2jGdBau7MUsYkEbp46BbBJHKpZmphPkTpg=="
}

resource "google_container_cluster" "gke-test-cluster" {
  name               = "${var.gcp_gke_name}"
  region             = "${var.gcp_region}"
  initial_node_count = 1

  master_auth {
    username = "${var.gcp_gke_superuser}"
    password = "${data.google_kms_secret.k8s_password.plaintext}"
  }

  network    = "${data.google_compute_network.gcp_network.self_link}"
  subnetwork = "${data.google_compute_network.gcp_network.subnetworks_self_links.0}"

  node_config {
    machine_type = "${var.gcp_gke_node_machine_type}"
    disk_size_gb = "${var.gcp_gke_node_machine_disk_size}"

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
