# Variables globales
variable "gcp_network" {
  default = "gcp-custom-network"
}

variable "gcp_region" {
  default = "europe-west1"
}

# Variables relacionadas con gke
variable "gcp_gke_name" {
  default = "gcp-gke-test"
}

variable "gcp_gke_superuser" {
  default = "superk8s"
}

variable "gcp_gke_node_machine_type" {
  default = "g1-small"
}

variable "gcp_gke_node_machine_disk_size" {
  default = "30"
}
