# Recursos relacionados con networking
data "google_project" "project" {}

# Creación de Cloud VPC
resource "google_compute_network" "gcp-network-1" {
  name        = "${var.gcp_network-1_name}"
  description = "${var.gcp_network-1_description}"

  routing_mode = "${var.gcp_network-1_routing_mode}"

  auto_create_subnetworks = "false"
  project                 = "${data.google_project.project.number}"
}

# Creación de subnetworks
resource "google_compute_subnetwork" "gcp-subnetwork-1" {
  name        = "${var.gcp_subnetwork-1_name}"
  description = "${var.gcp_subnetwork-1_description}"

  ip_cidr_range            = "${var.gcp_subnetwork-1_ip_cidr_range}"
  private_ip_google_access = "${var.gcp_subnetwork_private_gcp_access}"

  network = "${google_compute_network.gcp-network-1.self_link}"
}

resource "google_compute_subnetwork" "gcp-subnetwork-2" {
  name        = "${var.gcp_subnetwork-2_name}"
  description = "${var.gcp_subnetwork-2_description}"

  ip_cidr_range            = "${var.gcp_subnetwork-2_ip_cidr_range}"
  private_ip_google_access = "${var.gcp_subnetwork_private_gcp_access}"

  network = "${google_compute_network.gcp-network-1.self_link}"
}
