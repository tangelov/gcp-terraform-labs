# Variables relacionadas con networking

# Variables relacionadas con Cloud VPCs
variable "gcp_network-1_name" {
  default = "gcp-custom-network"
}

variable "gcp_network-1_description" {
  default = "Network personalizada para desplegar servicios en GCP"
}

variable "gcp_network-1_routing_mode" {
  default = "REGIONAL"
}

# Variables relacionadas con Subnetwork

# Globales
variable "gcp_subnetwork_private_gcp_access" {
  default = "true"
}

# Individuales
variable "gcp_subnetwork-1_name" {
  default = "gcp-custom-subnetwork-1"
}

variable "gcp_subnetwork-1_description" {
  default = "Subnetwork para desplegar máquinas virtuales dedicadas"
}

variable "gcp_subnetwork-1_ip_cidr_range" {
  default = "192.168.25.0/24"
}

variable "gcp_subnetwork-2_name" {
  default = "gcp-custom-subnetwork-2"
}

variable "gcp_subnetwork-2_description" {
  default = "Subnetwork para desplegar K8s"
}

variable "gcp_subnetwork-2_ip_cidr_range" {
  default = "192.168.50.0/24"
}
