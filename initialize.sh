#!/bin/bash

# Versión 1.0
# Este script se encarga de inicializar el bucket que va a almacenar el estado remoto de Terraform

# Comprovamos una serie de prerrequisitos: awk, sed y terraform
if ! [[ -x $(command -v awk) ]] || ! [[ -x $(command -v sed) ]] || ! [[ -x $(command -v terraform) ]]
then
	echo "Faltan requisitos por cumplir"
	exit 1
fi

# Variables a utilizar.
# El script se encarga de leer el nombre del proyecto y generar de forma dinámica un bucket utilizando el nombre del proyecto como base
GCP_PROJECT=$(awk '/project = "/,/"/' provider.tf | awk -F \" '{ print $2 }')
GCP_BUCKET="tf-state-${GCP_PROJECT}"
GCP_REGION=$(awk '/region  = "/,/"/' provider.tf | awk -F \" '{ print $2}')

# Realizamos algunas validaciones y replicamos la configuración del proyecto a otros ficheros
# Solo se modifica el fichero remote_state.tf si el bucket está vacío
if [[ ${GCP_PROJECT} == "" ]]
then
	echo "El proyecto está vacío. Rellene el fichero provider.tf con el nombre del proyecto antes de continuar"
	exit 1
else
	sed -i -e 's/bucket = ""/bucket = \"'"$GCP_BUCKET"'\"/g' remote_state.tf
fi

# Primero realizamos unas verificaciones simples:
# - Comprobamos si está instalado el cliente de gcloud
# - Relanzamos la configuración por si se quiere cambiar ésta: seleccionamos proyecto, región y zona
if  [[ $(command -v gcloud) ]]
then
	echo "¿Desea crear una nueva configuración de gcloud? (y/n)"
	read answer

	if [[ $answer == y ]]
	then
		gcloud init
	fi
else
	echo "gcloud no está instalado. Descarguelo de aquí: \"https://cloud.google.com/sdk/install\""
	exit 1
fi

# Habilitamos la API de Compute Engine para poder realizar operaciones con Terraform
gcloud services enable compute.googleapis.com

# Creamos el bucket que vamos a utilizar para almacenar el estado remoto de terraform
gsutil mb -l ${GCP_REGION} gs://${GCP_BUCKET}

# Inicializamos terraform y lanzamos el backend externo
# Se lanzará un link que nos autentica. Se utiliza la autenticación por defecto y no por llave puesto que está pensado sólo para testeo en estos laboratorios.
gcloud auth application-default login && terraform init

# Rehacemos el main.tf e importamos el bucket de GCS para Terraform
mv main.template main.tf

sed -i -e 's/name = ""/name = \"'"$GCP_BUCKET"'\"/g' main.tf
sed -i -e 's/location = ""/location = \"'"$GCP_REGION"'\"/g' main.tf

terraform import google_storage_bucket.gcp_gcs_bucket_terraform ${GCP_BUCKET}
