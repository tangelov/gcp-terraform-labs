# gcp-terraform-labs
Este código va a servir para realizar laboratorios que nos permitan profundizar nuestros conocimientos sobre GCP, Terraform, Kubernetes y todos sus parientes vivos.

## Requisitos
Los requisitos en Google Cloud Platform para aplicar este código son:

* Un proyecto previamente creado
* Un bucket para gestionar el estado de forma remota
* Una cuenta con permisos de administración

## Primeros pasos
Dentro del repositorio de git hay una serie de ficheros mínimos para que se pueda utilizar, en dos grupos fundamentales:

* Dos ficheros de Terraform (provider y remote\_state) que se corresponden con la configuración referente al proveedor y la configuración inicial de Terraform en un bucket remoto en GCS.

* Dos scripts en bash que se encargan de inicializar Terraform: _inicialize_ valida la configuración del proveedor y crear una configuración básica en un bucket de Google Cloud Storage y _component_ nos crea un arqueotipo para añadir nuevos elementos.

Para realizar comenzar un laboratorio de 0, debemos realizar los siguientes pasos:

```bash
# Descargamos la versión v1.0.0 del código y lo descomprimimos
wget https://gitlab.com/tangelov/gcp-terraform-labs/-/archive/v1.0.0/gcp-terraform-labs-v1.0.0.tar .

tar xvf gcp-terraform-labs-v1.0.0.tar && mv gcp-terraform-labs-v1.0.0 gcp-terraform-labs

# Editamos el fichero provider.tf y añadimos el proyecto donde vamos a desplegar
# Sustituimos (o definimos) el contenido de la variable GCP_PROJECT por el proyecto en el que vayamos a desplegar.

cd gcp-terraform-labs
sed -i -e 's/project = ""/project = \"'"$GCP_PROJECT"'\"/g' provider.tf

# Lanzamos el fichero de configuración y nos logueamos con nuestra cuenta
chmod +x initialize.sh && ./initialize.sh

# Este proceso crea un nuevo bucket y guarda el estado de Terraform en él. Por último lo importa
```

### Añadir nuevos elementos
Para añadir nuevos elementos debemos lanzar el otro script. Si por ejemplo queremos añadir un nuevo elemento que se llame networking, realizamos lo siguiente:

```bash
# Nos vamos a la raíz del código y ejecutamos el otro script con el nombre del elemento que queramos crear. Se nos generarán una especie de arquetipo a rellenar.

cd gcp-terraform-labs
chmod +x component.sh && ./component.sh networking

# Se nos generará un directorio con cuatro ficheros de los cuales solo tendremos que tocar main.tf para definir recursos y vars.tf para definir las variables a utilizar.
```

## Configuración avanzada
Los distintos recursos de Terraform y GCP tienen dependencias entre si que hace que sea necesario un cierto orden en el despliegue de recursos. De arriba hacia abajo, debemos hacerlo así:

* Redes y subredes
* Cloud KMS
* Cloud SQL
* Google Kubernetes Engine


## Gestión de secretos
A partir de la versión 1.2 del código se despliega un módulo de Cloud KMS. Podemos encontrar la documentación del servicio [aquí](https://cloud.google.com/kms/docs/).

En los ficheros de Terraform del servicio tan sólo generamos un anillo de claves y una llave simétrica de ejemplo, que podemos utilizar para generar secretos utilizando el nombre de ambos. Para la generación de estos secretos debemos utilizar la CLI de Google que habremos configurado tras utilizar el script de _initialize.sh_. [Aquí](https://www.terraform.io/docs/providers/google/d/google_kms_secret.html) tenemos un código de ejemplo:

```bash
#!/bin/bash
# Código para generar contraseñas

SENSIBLE="contraseñadenarices"
echo -n $SENSIBLE | gcloud kms encrypt \
 --location europe-west1 \
 --keyring key-ring \
 --key my-key \
 --plaintext-file - \
 --ciphertext-file - | bash64
```

Esto nos devolverá un texto cifrado que podremos utilizar para insertar secretos en Terraform sin que éstos estén visibles en nuestro código o nuestras variables. Aunque si estarán visibles dentro del _tfstate_ en texto plano, no debemos olvidarlo.
