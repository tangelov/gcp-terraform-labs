# Configuration of the GCP provider
provider "google" {
  project = ""
  region  = "europe-west1"
  zone    = "europe-west1-d"
}
