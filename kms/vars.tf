# Variables relacionadas con kms
variable "gcp_key_ring_name" {
  default = "gcp-test-lab-key-ring"
}

variable "gcp_key_ring_location" {
  default = "europe-west1"
}

variable "gcp_crypto_key_name" {
  default = "gcp-test-lab-crypto-key"
}
