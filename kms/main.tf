# Recursos relacionados con kms
data "google_project" "project" {}

resource "google_kms_key_ring" "gcp_key_ring" {
  name     = "${var.gcp_key_ring_name}"
  project  = "${data.google_project.project.id}"
  location = "${var.gcp_key_ring_location}"
}

resource "google_kms_crypto_key" "gcp_crypto_key" {
  name     = "${var.gcp_crypto_key_name}"
  key_ring = "${google_kms_key_ring.gcp_key_ring.id}"
}

output "gcp-test-lab-key" {
  value = "${google_kms_crypto_key.gcp_crypto_key.id}"
}
