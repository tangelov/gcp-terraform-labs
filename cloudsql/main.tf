# Recursos relacionados con cloudsql
data "google_project" "project" {}

data "google_compute_network" "gcp_network" {
  name = "${var.gcp_network}"
}

# Recursos importados de tfstates remotos -- DESCOMENTAR Y RELLENAR CORRECTAMENTE
# data "terraform_remote_state" "kms" {
#  backend = "gcs"
#
#  config {
#    bucket = ""
#    prefix = "kms/terraform.tfstate"
#  }
# }

data "google_kms_secret" "superadmin_password" {
  crypto_key = "${data.terraform_remote_state.kms.gcp-test-lab-key}"
  ciphertext = "CiQAHejTO9V7abCUajM7x+ESx6dYtqZ+8R7hEIpvHGJhJUsZ4j8SRwAvb0oe0eZ0i4Js6XRJ+edX6r6c4vqQdQ/3zdiaVj5iwfTLp12Ip9D57vMDlUjHKITopLd2eG0UtChLWzsoXe17vck/uxD3"
}

resource "google_sql_database_instance" "gcp-test-cloudsql" {
  name = "${var.gcp_cloudsql_name}"

  database_version = "${var.gcp_cloudsql_db_version}"
  region           = "${var.gcp_cloudsql_region}"

  settings {
    tier      = "${var.gcp_cloudsql_settings["tier"]}"
    disk_type = "${var.gcp_cloudsql_settings["disk_type"]}"

    ip_configuration {
      ipv4_enabled = "true"
    }
  }
}

resource "google_sql_database" "wordpress-1" {
  name      = "${var.gcp_cloudsql_wp_db_name}"
  instance  = "${google_sql_database_instance.gcp-test-cloudsql.name}"
  charset   = "${var.gcp_cloudsql_mysql_default_lang["charset"]}"
  collation = "${var.gcp_cloudsql_mysql_default_lang["collation"]}"
}

# Usuarios de Cloud SQL
resource "google_sql_user" "administrator" {
  name     = "${var.gcp_cloudsql_superadmin_user_name}"
  instance = "${google_sql_database_instance.gcp-test-cloudsql.name}"
  host     = "%"
  password = "${data.google_kms_secret.superadmin_password.plaintext}"
}
