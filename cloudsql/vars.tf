# Variables globales
variable "gcp_network" {
  default = "gcp-custom-network"
}

# Variables relacionadas con cloudsql
variable "gcp_cloudsql_name" {
  default = "gcp-test-cloudsql"
}

variable "gcp_cloudsql_db_version" {
  default = "MYSQL_5_7"
}

variable "gcp_cloudsql_region" {
  default = "europe-west1"
}

variable "gcp_cloudsql_settings" {
  default = {
    tier      = "db-f1-micro"
    disk_type = "PD_HDD"
  }
}

# Variables relacionadas con las bbdds
variable "gcp_cloudsql_mysql_default_lang" {
  default = {
    charset   = "utf8"
    collation = "utf8_general_ci"
  }
}

variable "gcp_cloudsql_wp_db_name" {
  default = "evaristio"
}

# Variables relacionadas con los usuarios
variable "gcp_cloudsql_superadmin_user_name" {
  default = "supertangelov"
}
